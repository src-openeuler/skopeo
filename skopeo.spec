%global with_debug 1

%if 0%{?with_debug}
%global _find_debuginfo_dwz_opts %{nil}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package %{nil}
%endif

ExcludeArch: ppc64

Name: skopeo
Epoch: 1
Version: 1.16.1
Release: 1
Summary: Work with remote images registries - retrieving information, images, signing content
License: Apache-2.0
URL: https://github.com/containers/skopeo
Source0: https://github.com/containers/skopeo/archive/refs/tags/v1.16.1.tar.gz
Source1: https://github.com/cpuguy83/go-md2man/archive/refs/tags/v2.0.3.tar.gz


BuildRequires: go-srpm-macros git-core pkgconfig(devmapper) make
BuildRequires: golang >= 1.19
BuildRequires: gpgme-devel libassuan-devel btrfs-progs-devel ostree-devel glib2-devel
Requires: containers-common shadow-subid-devel

%description
A command line utility that performs various operations on container images and image repositories

%package tests
Summary: Tests for %{name}

Requires: %{name} = %{epoch}:%{version}-%{release}
Requires: bats
Requires: gnupg
Requires: jq
Requires: golang
Requires: podman
Requires: crun
Requires: httpd-tools
Requires: openssl
Requires: fakeroot
Requires: squashfs-tools

%description tests
%{summary}

This package contains system tests for %{name}

%prep
%autosetup -Sgit -n %{name}-%{version} -p1
# The %%install stage should not rebuild anything but only install what's
# built in the %%build stage. So, remove any dependency on build targets.
sed -i 's/^install-binary: bin\/%{name}.*/install-binary:/' Makefile
sed -i 's/^completions: bin\/%{name}.*/completions:/' Makefile
sed -i 's/^install-docs: docs.*/install-docs:/' Makefile

tar -zxf %SOURCE1

%build
pushd go-md2man-*
go build -mod=vendor -o go-md2man .
export GOMD2MAN=$(realpath go-md2man)
popd

%set_build_flags
export CGO_CFLAGS=$CFLAGS

%ifarch x86_64
export CGO_CFLAGS="$CGO_CFLAGS -m64 -mtune=generic -fcf-protection=full"
%endif

BASEBUILDTAGS="$(hack/libdm_tag.sh) $(hack/libsubid_tag.sh)"

export BUILDTAGS="$BASEBUILDTAGS $(hack/btrfs_tag.sh) $(hack/btrfs_installed_tag.sh)"

# unset LDFLAGS earlier set from set_build_flags
LDFLAGS=''
export GO111MODULE=on
export GOFLAGS=-mod=vendor

go build -buildmode pie -compiler gc -tags="rpm_crashtraceback ${BUILDTAGS:-}" -ldflags "${LDFLAGS:-} -B 0x$(head -c20 /dev/urandom|od -An -tx1|tr -d '  \n') -extldflags '-Wl,-z,relro -Wl,-z,now '" -a -v -x -o bin/%{name} ./cmd/%{name}
%{__make} docs

%install
make \
    DESTDIR=%{buildroot} \
    PREFIX=%{_prefix} \
    install-binary install-docs install-completions

# system tests
install -d -p %{buildroot}/%{_datadir}/%{name}/test/system
cp -pav systemtest/* %{buildroot}/%{_datadir}/%{name}/test/system/

#define license tag if not already defined
%{!?_licensedir:%global license %doc}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/%{name}
%dir %{_datadir}/fish/vendor_completions.d
%{_datadir}/fish/vendor_completions.d/%{name}.fish
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_%{name}

%files tests
%license LICENSE
%{_datadir}/%{name}/test

%changelog
* Thu Sep 19 2024 tenglei <tenglei@kylinos.cn> - 1:1.16.1-1
- Type:Upgrade
- CVE:NA
- SUG:NA
- DESC:Update package to version 1.16.1, Bump Skopeo to v1.16.1, c/common to v0.60.2, c/image to v5.32.2

* Tue Aug 20 2024 tenglei <tenglei@kylinos.cn> - 1:1.16.0-1
- Type:Upgrade
- CVE:NA
- SUG:NA
- DESC:Update package to version 1.16.0,  Given the changes to the c/* projects underneath Skopeo

* Wed Jul 24 2024 tenglei <tenglei@kylinos.cn> - 1:1.15.2-1
- Type:Upgrade
- CVE:NA
- SUG:NA
- DESC:Update package to version 1.15.2, Fixes an interoperability issue while listing tags

* Fri Apr 12 2024 zhangbowei <zhangbowei@kylinos.cn> - 1:1.14.2-3
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-28180

* Fri Apr 12 2024 zhangbowei <zhangbowei@kylinos.cn> - 1:1.14.2-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC: fix CVE-2024-24786

* Fri Feb 23 2024 lijian <lijian2@kylinos.cn> - 1:1.14.2-1
- Upgrade to 1.14.2

* Tue Oct 17 2023 lijian <lijian2@kylinos.cn> - 1:1.8.0-2
- delete unused files

* Thu Apr  27 2023 Merlin <merlinsun@hust.edu.cn> - 1:1.8.0-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix infinite loop in parsing

* Mon Nov 07 2022 fushanqing <fushanqing@kylinos.cn> - 1:1.8.0-1
- update to 1.8.0.

* Thu Mar 24 2022 fushanqing <fushanqing@kylinos.cn> - 1:1.5.2-1
- update to 1.5.2 and remove subpackage containers-common

* Fri Dec 10 2021 wangqing <wangqing@uniontech.com> - 1.1.0-8.dev.git63085f5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:install seccomp.json for skopeo

* Thu Nov 25 2021 haozi007<liuhao27@huawei.com> - 1.1.0-7.dev.git63085f5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:install registries.conf for skopeo

* Thu Mar  18 2021 haozi007 <liuhao27@huawei.com> - 1.1.0-6.dev.git63085f5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:enable debug rpm

* Fri Feb  19 2021 haozi007 <liuhao27@huawei.com> - 1.1.0-5.dev.git63085f5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Change BuildRequires to source go-md2man

* Mon Feb  8 2021 haozi007 <liuhao27@huawei.com> - 1.1.0-4.dev.git63085f5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Change BuildRequires to golang
